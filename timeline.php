<?php  
  require 'head.php';
  require 'connection.php'; 
  $todos = $conn->query('select * from todo order by id desc');
  $todos = (object) $todos->fetch_all(MYSQLI_ASSOC);
?>


<div class="container py-5">
  <div class="main-timeline-4 text-white">

    <?php foreach ($todos as $key => $todo) : $todo = (object) $todo ?>

    <?php if($todo->id%2==0): ?>
    <div class="timeline-4 left-4">
      <div class="card gradient-custom">
        <div class="card-body p-4">
          <i class="fas fa-brain fa-2x mb-3"></i>
          <h4><?= $todo->nama ?></h4>
          <p class="small text-white-50 mb-4"><?= date('d F Y, H:i', strtotime($todo->tanggal)) ?> WIB</p>
          <p><?= substr($todo->deskripsi,0,100)?>...</p>
          <button class="badge bg-light text-black mb-0" onclick="detail(<?= $todo->id ?>)">More info</button>
        </div>
      </div>
    </div>
    <?php endif?>

    <?php if($todo->id%2!=0): ?>
    <div class="timeline-4 right-4">
      <div class="card gradient-custom-4">
        <div class="card-body p-4">
          <i class="fas fa-brain fa-2x mb-3"></i>
          <h4><?= $todo->nama ?></h4>
          <p class="small text-white-50 mb-4"><?= date('d F Y, H:i', strtotime($todo->tanggal)) ?> WIB</p>
          <p><?= substr($todo->deskripsi,0,100)?>...</p>
          <button class="badge bg-light text-black mb-0" onclick="detail(<?= $todo->id ?>)">More info</button>
        </div>
      </div>
    </div>
    <?php endif?>
  <?php endforeach; ?>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="Modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Keterangan</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function detail(id)
  {
    $.ajax({
      url:"<?= base_url('todolist_controller.php?api=') ?>" + id
    }).done((data)=>{
      $('.modal-body').html(data)
      $('#Modal').modal('show')
    })
  }
</script>