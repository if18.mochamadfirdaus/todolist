<?php  
  require 'head.php';
  require 'connection.php'; 
  $todos = $conn->query('select * from todo order by id desc');
  $todos = (object) $todos->fetch_all(MYSQLI_ASSOC);
?>

<style type="text/css">
  #list1 .form-control {
border-color: transparent;
}
#list1 .form-control:focus {
border-color: transparent;
box-shadow: none;
}
#list1 .select-input.form-control[readonly]:not([disabled]) {
background-color: #fbfbfb;
}
</style>

<section class="vh-100">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col">
        <div class="card" id="list1" style="border-radius: .75rem; background-color: #eff1f2;">
          <div class="card-body py-4 px-4 px-md-5">

            <p class="h1 text-center mt-3 mb-4 pb-3 text-primary">
              <i class="fas fa-check-square me-1"></i>
              <u>My Todo-s</u>
            </p>

            <div class="pb-2">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-row align-items-center">
                    <input id="todo_baru" type="text" class="form-control form-control-lg" id="exampleFormControlInput1"
                      placeholder="Tambah Todolist baru...">
                    <div>
                      <button id="tambah_todo" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Add</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <hr class="my-4">


            <a href="<?= base_url('timeline.php') ?>" class="btn btn-success btn-sm w-100 my-2" >Lihat Timeline</a>

            <table class="table table-striped">
              <tr>
                <th class="text-center">#</th>
                <th>Todo</th>
                <th class="text-center">Kategori</th>
                <th class="text-center">Prioritas</th>
                <th class="px-4">Tanggal</th>
                <th></th>
              </tr>

              <?php $no=1; foreach ($todos as $key => $todo): $todo = (object) $todo ?>
                <tr>
                  <td class="text-center"><?= $no ?></td>
                  <td><p class="lead fw-normal mb-0"><?= $todo->nama ?></p></td>
                  <td class="text-center">
                      <div class="p-2">
                        <span class="p-2 border border-<?php 
                          if($todo->kategori === 'Work') echo 'primary';
                          if($todo->kategori === 'Life') echo 'secondary';
                          if($todo->kategori === 'Family') echo 'success';
                          if($todo->kategori === 'Entertainment') echo 'info';
                       ?>">
                        <?= $todo->kategori ?>
                      </span>
                      </div>
                    </td>
                  <td class="text-center">
                    <span class="badge bg-<?= ($todo->prioritas==='High') ? 'danger' : (($todo->prioritas==='Middle') ? 'warning' : 'success') ?>">
                      <?= $todo->prioritas ?>
                        
                      </span>
                  </td>
                  <td class="text-center">
                   <li class="list-group-item px-3 py-1 d-flex align-items-center border-0 bg-transparent">
                <div
                  class="py-2 px-3 me-2 border border-warning rounded-3 d-flex align-items-center bg-light">
                  <p class="small mb-0">
                    <a href="#!" data-mdb-toggle="tooltip" title="Due on date">
                      <i class="fas fa-hourglass-half me-2 text-warning"></i>
                    </a>
                    <?= date('d F Y, H:i', strtotime($todo->tanggal)) ?> WIB
                  </p>
                </div>
              </li>
                  </td>
                  <td>
                    <div class="d-flex flex-row justify-content-end p-3">
                      <a href="<?= base_url('todolist_controller.php?delete='.$todo->id) ?>" class="text-danger" data-mdb-toggle="tooltip" title="Delete todo">
                        <i class="fas fa-trash-alt"></i>
                      </a>
                </div>
                  </td>
                </tr>
              <?php $no++; endforeach; ?>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Todolist</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('todolist_controller.php?insert=TRUE') ?>" method="post">
          <div class="mb-2">
            <label class="form-label text-muted">Nama Kegiatan</label>
            <input id="nama" type="text" class="form-control" name="nama" required="">     
          </div>

          <div class="mb-2">
            <label class="form-label text-muted">Kategori</label>
            <select class="form-control" name="kategori">
              <option value="Work">Work</option>
              <option value="Life">Life</option>
              <option value="Family">Family</option>
              <option value="Entertainment">Entertainment</option>
            </select>     
          </div>

          <div class="mb-2">
            <label class="form-label text-muted">Prioritas</label>
            <select class="form-control" name="prioritas">
              <option value="Low">Low</option>
              <option value="Middle">Middle</option>
              <option value="High">High</option>
            </select>     
          </div>
          
          <div class="mb-2">
            <label class="form-label text-muted">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" placeholder="Deskripsi" required=""></textarea>     
          </div>

          <div class="mb-2">
            <label class="form-label text-muted">Tanggal & Jam</label>
            <input type="datetime-local" name="tanggal" class="form-control" required="">
          </div>

          <div class="mb-2">
            <input type="submit" name="submit" class="btn btn-primary w-100">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#tambah_todo').click(function(){
      const A = $('#todo_baru').val();
      $('#nama').val(A)
  })
</script>